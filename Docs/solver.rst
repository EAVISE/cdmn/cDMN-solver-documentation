.. _solver:

cDMN Solver
===========

To be able to execute a spreadsheet, you need to use the cDMN solver.
The cDMN solver consists of two main parts:

1. A Python converter from cDMN to IDP language;
2. `The IDP system <https://dtai.cs.kuleuven.be/pages/software/idp/idp>`_ or `the newer IDP-Z3 system <https://www.IDP-Z3.be>`_, which are knowledge reasoning engines (can be run online or downloaded via Pip).

This is roughly shown on the figure below.

.. image:: ./Img/converter_diagram.png


.. _solver_installation:

1. Installation
---------------

The code for the solver is available at our `GitLab repository <https://gitlab.com/EAVISE/cdmn/cdmn-solver>`_.
It has been tested with Python 3.7=<. Other versions will probably work, but there's no guarantee.
The list of dependencies is as follows:

    * openpyxl
    * ply
    * numpy
    * python-dateutil
    * idp-solver

Installing the solver can be done via the Python repositories:

.. code:: bash

    $ pip install cdmn

This comes with a Python-based version of the IDP system, called `IDP-Z3 <http://www.IDP-Z3.be>`_. 
However, this version of IDP does not yet support all required functionalities (but it should cover most cases already).
If you have a cDMN model that requires these, you can install the IDP3 system (see :ref:`idp_install`)

.. _solver_usage:

2. Usage
--------

The main usage is as follows:

.. code:: bash

    usage: solver.py [-h] [-n name_of_sheet [name_of_sheet ...]] [-o outputfile]
                     [--idp idp] [--interactive-consultant-idp]
                     [--interactive-consultant-idp-z3] [--main]
                     [--errorcheck-overlap overlaptable]
                     [--errorcheck-shadowed shadowedtable]
                     [--errorcheck-rule ERRORCHECK_RULE]
                     [--errorcheck-gap ERRORCHECK_GAP]
                     path_to_file

    Run cDMN on DMN tables.

    positional arguments:
      path_to_file          the path to the xlsx or xml file

    optional arguments:
      -h, --help            show this help message and exit
      -n name_of_sheet [name_of_sheet ...], --name name_of_sheet [name_of_sheet ...]
                            the name(s) of the sheet(s) to execute
      -o outputfile, --outputfile outputfile
                            the name of the outputfile
      --idp idp             the path to the idp executable
      --interactive-consultant-idp
                            generate file specifically for the IDP3 Interactive
                            Consultant
      --interactive-consultant-idp-z3
                            generate file specifically for the IDP-Z3Interactive
                            Consultant
      --main                create a main, to use when generating for the IDP-Z3
                            Interactive Consultant
      --errorcheck-overlap overlaptable
                            the table to check for overlap errors: table is
                            identified by table id
      --errorcheck-shadowed shadowedtable
                            the table to check for shadowed rules: table is
                            identified by table id
      --errorcheck-rule ERRORCHECK_RULE
                            the rule to check for being erronous
      --errorcheck-gap ERRORCHECK_GAP
                            the table to check for input gaps: table is identified
                            by table id


For example, to execute the sheet named ``Problem1`` in the ``.xlsx`` file located at ``Data/DMNImplementation.xlsx``, the following command is used.

.. code:: bash
    
    $ python3 solver.py Data/DMNImplementation.xlsx -n "Problem1" -o "~/idp/Files/"


This will convert a sheet called ``Problem1`` from the ``DMNImplementation.xlsx`` file and save it in as ``~/idp/Files/Problem1.idp``.
It's also possible to set the name of the idp name yourself, by changing the output to ``~/idp/Files/custom_name.idp``.

If you wish to immediately execute the IDP specification, you can do so by supplying the ``--idp-z3`` flag.
This will automatically run the IDP-Z3 system, and print its output.

.. code:: bash

   $ python3 solver.py Data/DMNImplementation.xlsx -n "Problem1" -o "problem.idp" --idp-z3

Some cDMN specification can get large, so sometimes it makes sense to create a logical separation in different sheets.
It is possible to run multiple sheets together by supplying multiple names after the ``-n`` flag.


Create a file for the Interactive Consultant interface
______________________________________________________

The Interactive Consultant interface is an IDP-based, user-friendly interface which allows for straightforward interaction with a cDMN model.
In order to create a file that can be used in the interface, the ``interactive-consultant-idp`` and ``interactive-consultant-idp-z3`` flags can be used, depending on the IC version.

.. code:: bash
   
   $ python3 solver.py Data/DMNImplementation.xlsx -n "Problem1" -o "problem.idp" --interactive-consultant-idp-z3

In the case that a file is being generated for use in the IDP-Z3 webIDE, the ``--main`` flag should also be supplied.

Use DMN as input
________________

If you wish to execute DMN XML instead of a spreadsheet, you can do so by simply supplying the XML file as input.

Otherwise, it is also possible to create DMN tables inside a spreadsheet.
In this case however, it is still necessary to create a glossary for the DMN specification.
This can easily be done by filling out the Constant glossary table with all the variable names.



.. _idp_install:

3 Installation of the IDP system
--------------------------------

.. NOTE::
   For most use cases, the IDP-Z3 system installed with the cDMN package will suffice.
   If your use case requires advanced functionalities, this section explains how to install the legacy IDP.

There's two options for the installation of the IDP system:

    1. Use the `online version <http://verne.cs.kuleuven.be/idp/server.html>`_, which is limited in input-output and processor usage.
    2. Use an offline version, which is limited only by your own hardware (but only available for Linux or Mac).

If you want to only use the online version, you can skip the rest of this section.
Most of the time the online version should be fine.
Once the problems start requiring more CPU power, or if the server's unreachable, the offline version is the only way to run solve the problems.

To install the offline version, two components are needed: the actual IDP system, and the web interface for the system.
A guide on downloading installing these can be found on the `IDP website <https://dtai.cs.kuleuven.be/pages/software/idp/try>`_.

Run the IDP system
__________________

To execute an IDP file, open the webIDE by running the ``webID`` file or by surfing to `the online IDE <https://verne.cs.kuleuven.be/idp/server.html>`_.
The offline version should show up at ``localhost:4004``.
If you saved your IDP file in the work folder of the IDE, it should also show up under ``Pick File``.

If you're using the online version, simply opening the IDP file in your favorite text editor and copy and pasting the contents to the IDE should be enough.
