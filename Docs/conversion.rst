
.. role:: raw-html(raw)
    :format: html

.. _conversion:

cDMN to FO(:raw-html:`&#183`) conversion
========================================


This page details the full cDMN to FO(:raw-html:`&#183`) conversion.
This conversion is done in two steps; first from cDMN to Python objects, then from these objects to the `IDP-Z3 system <https://www.IDP-Z3.be>`_.
In :ref:`cdmn_to_python`, an explanation is given for the translation of cDMN to Python objects.
In :ref:`python_to_idp`, the conversion of Python to FO(:raw-html:`&#183`) is explained.
An example of what the result of the conversion looks like is given in :ref:`cdmn_to_idp`.

The basic conversion can be seen in the following image.

.. image:: Img/converter.png

.. _cdmn_to_python:

1. cDMN to Python conversion
----------------------------

The first step in the conversion to Python objects, is for Python to open the
:code:`.xlsx` file.
This is done using the `OpenPyXL <https://openpyxl.readthedocs.io/en/stable/>`_
package.

The solver reads all the tables, organises them by type and then parses them in
the following order:

1. Sub-glossary tables
2. Data tables
3. Decision tables and Constraint tables
4. The goal table

1.1 Glossarries
___________________

The glossaries are parsed in the following order:

1. Type
2. Function
3. Constant
4. Relation
5. Boolean

The order of the latter four doesn't really matter, but it is necessary to
always parse the Type sub-glossary first.
Parsing this sub-glossary creates Python :code:`type` objects.
These objects contain a name, their cDMN supertype and the possible values for
that type.

After creating a :code:`type` object for every row in the Type sub-glossary,
the other sub-glossaries are parsed next.
Functions and relations need to be able to know what types they contain in
their name, hence the need to parse the types first.

1.2 Data tables
_______________

The values in data tables are stored in dictionaries, to make it easier to
format them later on for the IDP system.
When interpreting the values for a type in a data table, there exist two possiblities.
Either the :code:`Values` column of the type was filled out in the
sub-glossary with values, or it contained a reference to a data table such as ``see Data Table x``.
In the former case, each value in the data table is checked on validity.
If for instance value :code:`4` is found for a type which defined its range as
:code:`[5..10]`, the solver throws an error.
In the latter case, every value found in the data table is added to the type's
list of possible values.

1.3 Decision tables and Constraint tables
_________________________________________

Decision and constraint tables are represented in a simple way.
They are converted into :code:`Table` objects, which contain a list of inputs,
a list of outputs, the list of rows, their name and their hit policy.
No more information is necessary in order to represent these tables in Python.

1.4 Goal table
______________

The goal table is the easiest of all: when it's read, it simply creates a
dict which contains the inference method (either model expand, minimize or
maximize) and the amount of requested models.


.. _python_to_idp:

2. Python to IDP conversion
---------------------------

Converting the Python objects to IDP is done in a certain order.
For the IDP system, five different blocks of code need to be generated:

1. Vocabulary
2. Structure
3. Theory
4. Term
5. Main

The order of this code does not really matter, as long as the main is at last
code block.

2.1 Vocabulary
______________

The vocabulary block contains the definitions for the types, functions and predicates used in the IDP model.
To create this block, every relevant Python object has a :code:`to_idp_voc`
method to generate the relevant code.
For instance, for :code:`Type` objects, this returns the type name followed by its domain of values.
Relations (and booleans) are formatted as :code:`name: (arg0 * arg1 * ...) -> Bool`.
Functions (and constants) are formatted as :code:`name: (arg0 * arg1 * ...): -> argn`.

2.2 Structure
_____________

The structure defines all the possible values for the known types, as well as
the satisfied predicates and fully known functions.
For this, each :code:`Type` and :code:`Predicate` object has a
:code:`to_idp_struct` method.
For types, this generates :code:`typename := values`.

For relations, this is of the form :code:`relation_name := { }`, with the sets of argument values separated by commas.
For functions, each sets of arguments also lists another value separated by an :code:`->`.



.. _theory:

2.3 Theory
__________

The theory contains the different rules.
To create the theory, each :code:`Table` object is translated based on their
hit policy.

.. csv-table::
    :header: "Hit Policy", "IDP concept"

    "U", "Definitions"
    "F", "Definitions"
    "A", "Definitions"
    "C+, C#, C<, C>", "Aggregates"
    "E*", "Constraints"

2.4 Main
_________________

The main block is fully created from the goal table.

The main is mostly the same every time, except for two parameters: the
inference task (model expansion, optimization or propagation) and the amount of
models.


.. _cdmn_to_idp:

3. cDMN to IDP example
----------------------

As an example, we show the conversion of the Who Killed Agatha challenge.
First we start off with the translation of the glossary:

.. raw:: html

    <table class="glos">
    <tr>
    <th class="glos-title" colspan="3">Type</th>
    </tr>
    <tr>
    <td
    class="glos-header">Name</td>
    <td
    class="glos-header">Type</td>
    <td
    class="glos-header">Values</td>
    </tr>
    <tr>
    <td
    class="glos-td">Number</td>
    <td
    class="glos-td">int</td>
    <td
    class="glos-td">[0..100]</td>
    </tr>
    <tr>
    <td
    class="glos-td">Person</td>
    <td
    class="glos-td">string</td>
    <td
    class="glos-td">Agatha, Butler, Charles</td>
    </tr>
    </table>
    <br>
    <table
    class="glos">
    <tr>
    <th
    class="glos-title"
    colspan="2">Function</th>
    </tr>
    <tr>
    <td
    class="glos-header">Name</td>
    <td
    class="glos-header">Type</td>
    </tr>
    <tr>
    <td
    class="glos-td">Number
    hated
    persons
    of
    Person</td>
    <td
    class="glos-td">Number</td>
    </tr>
    </table>
    <br>

    <table class="glos">
    <tr>
    <th class="glos-title" colspan="1">Relation</th>
    </tr>
    <tr>
    <td
    class="glos-header">Name</td>
    </tr>
    <tr>
    <td
    class="glos-td">Person
    hates
    Person</td>
    </tr>
    <tr>
    <td
    class="glos-td">Person
    richer
    than
    Person</td>
    </tr>
    </table>
    <br>

    <table class="glos">
    <tr>
    <th class="glos-title" colspan="2">Constant</th>
    </tr>
    <tr>
    <td
    class="glos-header">Name</td>
    <td
    class="glos-header">Type</td>
    </tr>
    <tr>
    <td
    class="glos-td">Killer</td>
    <td
    class="glos-td">Person</td>
    </tr>
    </table>
    <br>
    

The glossary tables translate into ``Vocabulary`` block of the IDP system.
The above tables would result in the following vocabulary:

.. code::

    Vocabulary V {
        type Person constructed from { Agatha, Butler, Charles } 
        type Number = { 0..100 } isa int

        Hated_persons(Person): Number
        _Hated_persons(Person): Number
        
        Killer: Person

        Person_hates_Person(Person, Person)
        Person_richer_than_Person(Person, Person)
    }

We can see that every row in the glossary results into a row in the vocabulary.
The only exception to this are functions which are used in aggregates such as ``C#``: they also need an auxiliary function to deal with a bug in the IDP system.
This auxiliary function has the same name, but start with an underscore (as can be seen here by ``Hated_persons`` and ``_Hated_persons``).

All the other glossary elements underwent obvious transformations:

    * String types are now types which use ``constructed from``.
    * Int types use ``= {x..y} isa int`` as their definition.
    * Functions are translated into ``Func_name(Arg1, Arg2, ...): Return_Value``.
    * Constants are translated into ``Const_name: Return_Value``.
    * Relations are translated into ``Rel_name(Arg1, Arg2, ...)``.
    * Booleans are translated into ``Boolean_name``.

.. raw:: html

    <table class="dec">
    <tr>
    <th class="dec-title" colspan="1">Rule 1</th>
    <th class="dec-empty"></th>
    <th class="dec-empty"></th>
    </tr>
    <tr>
    <td
    class="dec-td">E*</td>
    <td
    class="dec-output">Killer
    hates
    Agatha</td>
    <td
    class="dec-output">Killer
    richer
    than
    Agatha</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">Yes</td>
    <td
    class="dec-td">No</td>
    </tr>
    </table>
    <br>

Constraint tables are also translated very clearly.
As the table in :ref:`theory` shows, they are translated into implications.
The above table translates to the following in the IDP system:

.. code::

        //A killer always hates, and is no richer than his victim
        true => Person_hates_Person(Killer, Agatha) & ~(Person_richer_than_Person(Killer, Agatha)).

This is a very straightforward translation.
Note that, because there is no input column, the implication is always true.

.. raw:: html



    <table class="dec">
    <tr>
    <th class="dec-title" colspan="2">Agatha hates
    everybody but the butler</th>
    <th class="dec-empty"></th>
    </tr>
    <tr>
    <td
    class="dec-td">E*</td>
    <td
    class="dec-input">Person</td>
    <td
    class="dec-output">Agatha
    hates
    Person</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">Butler</td>
    <td
    class="dec-td">No</td>
    </tr>
    <tr>
    <td
    class="dec-td">2</td>
    <td
    class="dec-td">not(Butler)</td>
    <td
    class="dec-td">Yes</td>
    </tr>
    </table>
    <br>

This is also a very straightforward table to translate.
It translates into the following:

.. code::

        //Agatha hates everybody but the butler
        !Person[Person]: Person ~= Butler => Person_hates_Person(Agatha, Person).

        !Person[Person]: Person = Butler => ~(Person_hates_Person(Agatha, Person)).

.. raw:: html

    <table class="dec">
    <tr>
    <th class="dec-title" colspan="3">The butler hates
    everyone not richer than Agatha</th>
    <th class="dec-empty"></th>
    </tr>
    <tr>
    <td
    class="dec-td">E*</td>
    <td
    class="dec-input">Person</td>
    <td
    class="dec-input">Person
    richer
    than
    Agatha</td>
    <td
    class="dec-output">Butler
    hates
    Person</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">-</td>
    <td
    class="dec-td">No</td>
    <td
    class="dec-td">Yes</td>
    </tr>
    </table>
    <br>

    <table class="dec">
    <tr>
    <th class="dec-title" colspan="3">Charles hates no one
    that Agatha hates</th>
    <th class="dec-empty" colspan="2"></th>
    </tr>
    <tr>
    <td
    class="dec-td">E*</td>
    <td
    class="dec-input">Person</td>
    <td
    class="dec-input">Agatha
    hates
    Person</td>
    <td
    class="dec-output">Charles
    hates
    Person</td>
    <td
    class="dec-output">Butler
    hates
    Person</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">-</td>
    <td
    class="dec-td">Yes</td>
    <td
    class="dec-td">No</td>
    <td
    class="dec-td">Yes</td>
    </tr>
    </table>
    <br>



.. raw:: html

    <table class="dec">
    <tr>
    <th class="dec-title" colspan="4">Count
    enemies</th>
    <th class="dec-empty"></th>
    </tr>
    <tr>
    <td
    class="dec-td">C#</td>
    <td
    class="dec-input">Person
    called
    p1</td>
    <td
    class="dec-input">Person
    called
    p2</td>
    <td
    class="dec-input">p1
    hates
    p2</td>
    <td
    class="dec-output">Number
    hated
    persons
    of
    p1</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">-</td>
    <td
    class="dec-td">-</td>
    <td
    class="dec-td">yes</td>
    <td
    class="dec-td">p2</td>
    </tr>
    </table>
    <br>


The above table is a bit harder to translate than the constraint tables.
Because of a bug in aggregates in the IDP system, we have to work using an auxiliary function.
The actual count is done in the first line.

.. code::

        //Count enemies
        !p1[Person]:  _Hated_persons(p1) = sum(#{  p2[Person] : Person_hates_Person(p1, p2) & p1 = p1 & true }).
        {
            !p1[Person]: Hated_persons(p1) = _Hated_persons(p1).
        }

.. raw:: html

    <table
    class="dec">
    <tr>
    <th
    class="dec-title"
    colspan="2">Noone
    hates
    all</th>
    <th
    class="dec-empty"></th>
    </tr>
    <tr>
    <td
    class="dec-td">E*</td>
    <td
    class="dec-input">Person</td>
    <td
    class="dec-output">Number
    hated
    persons
    of
    Person</td>
    </tr>
    <tr>
    <td
    class="dec-td">1</td>
    <td
    class="dec-td">-</td>
    <td
    class="dec-td">&lt
    3</td>
    </tr>
    </table>
    <br>

The full translation of the entire cDMN implementation is shown next.
Note how the ``Structure`` block is empty, because no data table was used.

.. code::


    Vocabulary V {
        type Person constructed from { Agatha, Butler, Charles } 
        type Number = { 0..100 } isa int

        Hated_persons(Person): Number
        _Hated_persons(Person): Number
        
        Killer: Person

        Person_hates_Person(Person, Person)
        Person_richer_than_Person(Person, Person)
    }

    Structure S:V {
    }
    Theory T:V {
        //A killer always hates, and is no richer than his victim
        true => Person_hates_Person(Killer(), Agatha) & ~(Person_richer_than_Person(Killer(), Agatha)).

        //Agatha hates everybody but the butler
        !Person[Person]: Person ~= Butler => Person_hates_Person(Agatha, Person).

        !Person[Person]: Person = Butler => ~(Person_hates_Person(Agatha, Person)).

        //Charles hates no one that Agatha hates
        !Person[Person]: Person_hates_Person(Agatha, Person) => ~(Person_hates_Person(Charles, Person)) & Person_hates_Person(Butler, Person).

        //Butler hates everyone not richer than Agatha
        !Person[Person]: ~(Person_richer_than_Person(Person, Agatha)) => Person_hates_Person(Butler, Person).

        //Count enemies
        !p1[Person]:  _Hated_persons(p1) = sum(#{  p2[Person] : Person_hates_Person(p1, p2) & p1 = p1 & true }).
        {
            !p1[Person]: Hated_persons(p1) = _Hated_persons(p1).
        }
        //Noone hates all
        !Person[Person]: true => Hated_persons(Person) < 03.

    }


    procedure main(){
    stdoptions.nbmodels = 1
    printmodels(modelexpand(T,S))
    }
