.. _examples:

Examples
========

This page lists some examples for cDMN implementations.
For now, most examples are based on `DMCommunity challenges <https://dmcommunity.org/challenge/>`_.
In the future, other examples will be added as well.

The following table has a link for every example, together with a short list of the used concepts in the example.

.. csv-table:: cDMN Examples
   :header: "Example", "Concepts"

    :ref:`who_killed_agatha`, "Quantification, Constraints, Functions"
    :ref:`vacation_days`, "Quantification, Functions, Relations"
    :ref:`vacation_days_advanced`, "Quantification, Functions, Relations, Data Table, Optimization"
    :ref:`hamburger_challenge`, "Quantification, Constraints, Data Table, Functions, Constants, Optimization"
    :ref:`monkey_business`, "Quantification, Constraints, Functions"
    :ref:`change_making`, "Quantification, Constants, Optimization"
    :ref:`doctor_planning`, "Quantification, Constraints, Functions, Relations, Optimization, Data Table"
    :ref:`balanced_assignment`, "Quantification, Functions"
    :ref:`map_coloring`, "Quantification, Constraints, Functions, Relations, Data Table"
    :ref:`map_coloring_advanced`, "Quantification, Constraints, Functions, Relations, Data Table, Optimization"
    :ref:`crack_the_code`, "Quantification, Constraints, Functions, Constants, Complex Maths"
    :ref:`zoo_buses_and_kids`, "Quantification, Constraints, Functions, Constants, Optimization"
    :ref:`define_duplicate_product_lines`, "Quantification, Functions, Relation, Data Table"
    :ref:`calculator`, "Quantification, Functions, Constants, Optimization"
    :ref:`virtual_chess_tournament`, "Quantification, Functions, Constraints, Aggregates"
    :ref:`set`, "Quantification, Functions, Relations, Data Table, Goal Table, Constraints, Aggregates"
    :ref:`covid_testing`, "Booleans, Constants, Aggregates"
    :ref:`where_is_gold`, "Relations, Constants, Aggregates"
    :ref:`department_employees`, "Functions, Constants, Relations, Aggregates, Quantification"
