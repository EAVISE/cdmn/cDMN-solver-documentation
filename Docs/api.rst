.. _api:

The cDMN developer reference
====================================

This page details the reference for the cDMN solver.

In a nutshell, the solver consists of 5 elements:

    * An Excel to numpy converter ( :ref:`cdmn_solver`).
    * :ref:`glossary`, which contains all types and predicates.
    * :ref:`interpret`.
    * :ref:`table`, which represents a DMN+ table.
    * :ref:`idply`, which translates DMN notation inside cells to IDP notation.
    * :ref:`parse_xml`, a class to convert DMN XML into cDMN tables.
    * :ref:`post_process`, functions used for post processing generated configurations.


.. toctree::
   :maxdepth: 3
   :caption: Contents: 

   cdmn_solver
   DMN_api
   glossary
   interpret
   table
   table_operations
   idply
   parse_xml
   post_process

