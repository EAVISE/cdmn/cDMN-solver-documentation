.. _vacation_days_advanced:

Vacation Days Advanced
----------------------

This example is an advanced version of a previous example, :ref:`vacation_days`. 
The full specification can be found here: `Vacation Days Advanced <https://dmcommunity.org/challenge/challenge-nov-2018/>`_.
In this challenge, we need to calculate vacation days for employees, based on their age and years of service and some more information.
In total, there are seven rules:

.. admonition:: Vacation Days

    1) Every employee receives at least 22 vacation days.

    2) Employees younger than 18 or at least 60 years, or employees with at least 30 years of service can receive extra 5 days.

    3) Employees with at least 30 years of service and also employees of age 60 or more, can receive extra 3 days, on top of possible additional days already given.

    4) If an employee has at least 15 but less than 30 years of service, extra 2 days can be given. These 2 days can also be provided for employees of age 45 or more.

    5) A college student is eligible to 1 extra vacation day.

    6) If an employee is a veteran, 2 extra days can be given.

    7) The total number of vacation days cannot exceed 29.

The seventh rule of the specification is what makes this challenge so interesting.
At first, it would seem intuitive to place a soft maximum on the vacation days: if an employee exceeds 29 days, give them 29 days.
However, in `Jacob Feldman's OpenRules implementation <https://dmcommunity.files.wordpress.com/2018/12/OpenRules-Solutions-for-Vacation-Days-Advanced.pdf>`_ he points out that this would mean we can simply subtract days from some types and only give partial number of eligible days.
If we assume that this is not allowed, an employee would not want to make use of every type of vacation days for which they are eligible, but only the ones that get their total closest to 29.
In this small example this does not matter too much, but in bigger systems with more mutually exclusive rules this could make a difference.

In this implementation, we will deal with two types of vacation rules: the ones for which an employee is eligible, and the ones that they 'apply', e.g. actually use.
In other words, they will not have to use all vacation days for which they are eligible.
We will try to pick the used days in such a way that a maximum number is achieved for every employee.

To fill out our glossary, we start by adding types for the vacation days, age, service years, the employees and the different rules.
For the example, we create three dummy employees: Huey, Dewey and Louie.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Vacation Days</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..1000]</td>
        </tr>
        <tr>
            <td class="glos-td">Age</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
        <tr>
            <td class="glos-td">Service Years</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
        <tr>
            <td class="glos-td">Employee</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Huey, Dewey, Louie</td>
        </tr>
        <tr>
            <td class="glos-td">Rule</td>
            <td class="glos-td">String</td>
            <td class="glos-td">r2, r3, r4, r5, r6</td>
        </tr>
    </table>
    <br>

Next, we create functions to map every employee to their age, service years and total vacation days.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">age of Employee</td>
            <td class="glos-td">Age</td>
        </tr>
        <tr>
            <td class="glos-td">service years of Employee</td>
            <td class="glos-td">Service Years</td>
        </tr>
        <tr>
            <td class="glos-td">vacation days of Employee </td>
            <td class="glos-td">Vacation Days</td>
        </tr>
    </table>
    <br>
    
In order to express whether an employee is a veteran or student, we add 2 relations.
We also add a relation which keeps track of the eligible rules, and the 'applied' rules.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is Student</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is Veteran</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is eligible for Rule</td>
        </tr>
        <tr>
            <td class="glos-td">Employee applies Rule</td>
        </tr>
    </table>
    <br>

Now that our glossary is complete, we can start creating decision and constraint tables.
We start by implementing a table for every rule, to express when an exmployee is eligible.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Define the eligibility for rule 2</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">A</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&lt; 18</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&ge; 60</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[18, 60)</td>
            <td class="dec-td">&ge; 30</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. raw:: html

   <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Define the eligibility for rule 3</th>
            <th class="dec-empty" ></th>
        </tr>
        <tr>
            <td class="dec-td">A</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r3</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&ge; 30</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&lt; 60</td>
            <td class="dec-td">&lt; 30</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. raw:: html

   <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Define the eligibility for rule 4</th>
            <th class="dec-empty" ></th>
        </tr>
        <tr>
            <td class="dec-td">A</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r4</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[15, 30)</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&ge; 45</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. raw:: html

   <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Define the eligibility for rule 5</th>
            <th class="dec-empty" ></th>
        </tr>
        <tr>
            <td class="dec-td">A</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">Employee is Student</td>
            <td class="dec-output">Employee is eligible for r5</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. raw:: html

   <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Define the eligibility for rule 6</th>
            <th class="dec-empty" ></th>
        </tr>
        <tr>
            <td class="dec-td">A</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">Employee is Veteran</td>
            <td class="dec-output">Employee is eligible for r6</td>
        </tr>
        <tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

Now that we can decide which employee is eligible for what rules, we need a way to calculate the number of days.
We do this by creating a :code:`C+` table, in which we sum the number of days per rule based on which rules are applied.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="7">Calculate vacation days based on which rules are applied.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">Employee applies r2</td>
            <td class="dec-input">Employee applies r3</td>
            <td class="dec-input">Employee applies r4</td>
            <td class="dec-input">Employee applies r5</td>
            <td class="dec-input">Employee applies r6</td>
            <td class="dec-output">vacation days of Employee</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">22</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">5</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">3</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">-</td>
            <td class="dec-td">No</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">2</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

Just stating that rules can be applied is of course not enough.
We need to make sure that only rules for which an employee is eligible can be applied.
In order to do this, we create a constraint table.
Note how it states that a rule can only be applied if the employee is eligible, but that it does not state the opposite.
In other words, not every eligible rule needs to be applied.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Only apply rule if eligible.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">Rule</td>
            <td class="dec-input">Employee applies Rule</td>
            <td class="dec-output">Employee is eligible for Rule</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

Of course, we also need to add a constraint that no employee can have more than 29 vacation days.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Max days.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Employee</td>
            <td class="dec-output">vacation days of Employee</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&le; 29</td>
        </tr>
    </table>
    <br>

There we have it!
We are now able to calculate vacation days for employees.
However, just this calculation alone does not do much yet.
We need to be able to look for the solution in which the employees have a maximum number of vacation days.
To do this, we sum all the vacation days of employees and maximize it.
This is a bit unfortunate, but because we added quantification we have to maximize for all employees.
If we did not use quantification, we could run the model once for every employee.
In this case the employees do not affect each other, so solving the problem for all of them at the same time creates no implications.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Total days.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Employee</td>
            <td class="dec-output">Total Days</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">vacation days of Employee</td>
        </tr>
    </table>
    <br>


.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Maximize Total Days </td>
        </tr>
    </table>
    <br>


To now check the amount of vacation days our employees get, we can insert a data table containing their information, and run the solver.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Data Table Employee Info</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-input">Employee</td>
            <td class="dec-output">age of Employee</td>
            <td class="dec-output">service years of Employee</td>
            <td class="dec-output">Employee is Veteran</td>
            <td class="dec-output">Employee is Student</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Huey</td>
            <td class="dec-td">17</td>
            <td class="dec-td">1</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Dewey</td>
            <td class="dec-td">70</td>
            <td class="dec-td">31</td>
            <td class="dec-td">No</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Louie</td>
            <td class="dec-td">35</td>
            <td class="dec-td">16</td>
            <td class="dec-td">No</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>


This gives us the following solution:

.. code:: bash

    Number of models: 1
    Model 1
    =======
    structure  : V {
      Employee_applies_Rule = { Dewey,r2; Dewey,r5; Huey,r2; Huey,r6; Louie,r3; Louie,r4 }
      Employee_eligible_for_Rule = { Dewey,r2; Dewey,r3; Dewey,r4; Dewey,r5; Huey,r2; Huey,r3; Huey,r6; Louie,r3; Louie,r4 }
      Employee_is_Student = { Dewey }
      Employee_is_Veteran = { Huey }
      Age = { Dewey->70; Huey->17; Louie->35 }
      Service_Years = { Dewey->31; Huey->1; Louie->16 }
      Total_Days = 83
      Vacation_Days = { Dewey->28; Huey->28; Louie->27 }
    }

    Elapsed Time:
    0.111631
