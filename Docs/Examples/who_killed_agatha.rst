.. _who_killed_agatha:

Who Killed Agatha
-----------------

The Who Killed Agatha example can be found at the `DMCommunity website <https://dmcommunity.org/challenge/challenge-nov-2014/>`_.


.. admonition:: Who killed Agatha

    Someone in Dreadsbury Mansion killed Aunt Agatha. Agatha, the butler, and Charles live in Dreadsbury Mansion, and are the only ones to live there. A killer always hates, and is no richer than his victim. Charles hates noone that Agatha hates. Agatha hates everybody except the butler. The butler hates everyone not richer than Aunt Agatha. The butler hates everyone whom Agatha hates. Noone hates everyone. Who killed Agatha?

We can convert the challenge description into the following six clear rules:

1. A killer always hates, and is no richer than it’s victim.
2. Charles hates noone that Agatha hates.
3. Agatha hates everybody but the butler.
4. The butler hates everyone not richer than Agatha.
5. The butler hates everyone whom Agatha hates.
6. Noone hates everyone.


The first step when designing a cDMN solution, is creating the glossary.
In our glossary, we define all the elements which we'll use in our cDMN implementations.

After having read the description of the challenge, it should be clear that we need at least a ``Person`` type.
This type will serve as the domain containing all the people's names.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Person</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Agatha, Butler, Charles</td>
        </tr>
    </table>
    <br>


We also need a way to declare whether or not a person is hated by another. 
Since a person can hate multiple people, it is not possible to use a function in this situation.
Thus, we introduce a relation instead: ``Person hates Person``.
The same goes for declaring whether or not a person is richer than another.
For this, we introduce the relation ``Person richer than Person``.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Person hates Person</td>
        </tr>
        <tr>
            <td class="glos-td">Person richer than Person</td>
        </tr>
    </table>
    <br>


Lastly, we also need a way to find out who our killer is.
A constant lends itself perfectly for this, since there is exactly one killer.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Killer</td>
            <td class="glos-td">Person</td>
        </tr>
    </table>
    <br>

Assuming that our glossary is finished, we can start modeling each of the rules.

.. admonition:: Rule 1

    **A killer always hates, and is no richer than its victim.**

Here we need to specify that a killer hates its victim as well as isn't richer than the victim.
This can easily be done using a constraint table.
We state that the Killer has to hate Agatha, and cannot be richer than her.
Note how it is possible to construct a constraint table without input variables.
In this case, the outputs are always applicable.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Rule 1</th>
            <th class="dec-empty"></th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">Killer hates Agatha</td>
            <td class="dec-output">Killer richer than Agatha</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>

*"Killer hates Agatha" and "Killer richer than Agatha" need to be true*.


.. admonition:: Rule 2

    **Charles hates noone that Agatha hates.**

This rule is also best implemented using a constraint table.
We can state that for every person, if Agatha hates them, then Charles doesn't.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Charles hates no one that Agatha hates</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">Agatha hates Person</td>
            <td class="dec-output">Charles hates Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>

*For every person for whom "Agatha hates Person" is true, "Charles hates Person" is false.*

.. admonition:: Rule 3

    **Agatha hates everybody but the butler.**

This can again be modeled using a constraint table.
We evaluate each person, and if they are not the butler, Agatha hates them (row 1).
If they are the butler, Agatha doesn't hate them (row 2).

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Agatha hates everybody but the butler</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Person</td>
            <td class="dec-output">Agatha hates Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Butler</td>
            <td class="dec-td">No</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">not(Butler)</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule 4

    **The butler hates everyone not richer than Agatha.**


We use another constraint table for this rule.
We evaluate each person not richer than Agatha, and have the Butler hate that person.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">The butler hates everyone not richer than Agatha</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">Person richer than Agatha</td>
            <td class="dec-output">Butler hates Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">No</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule 5
    
    **The butler hates everyone whom Agatha hates.**

Here we quantify over each person whom Agatha hates, and make "Butler hates Person" true.
Coincidentally, we already have a table which has the same input.
We can add an extra output column to "Charles hates noone that Agatha hates" and save a table this way.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Charles hates no one that Agatha hates</th>
            <th class="dec-empty" colspan="2"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">Agatha hates Person</td>
            <td class="dec-output">Charles hates Person</td>
            <td class="dec-output">Butler hates Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule 6
    
    **Noone hates everyone.**

For this rule, we need to define a way to know who "everyone" is, and how to model this.
A possible solution is to introduce a new function, which will keep track of how many people are hated by one person.
For instance, we could extend our ``types`` glossary with a type called ``Number`` and then create a ``Function`` glossary, containing a function to keep track of the number of hated people for each person.

.. raw:: html

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Number</td>
            <td class="glos-td">int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
    </table>
    <br>
    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">number hated persons of Person</td>
            <td class="glos-td">Number</td>
        </tr>
    </table>
    <br>

This would then allow us to create two tables: one counting how many persons are hated, and one putting a constraint on that amount.
To count how many are hated, we can make use of the ``C#`` hitpolicy.
We need to evaluate every p1, and count the amount of p2 whom p1 hates.
Afterwards, we use a ``E*`` table to make sure nobody hates more than 3 people.
In cDMN, the ``#Type`` operator can be used to count the number of elements in a type.
In this case, we use ``#Person`` to denote the number of people in the problem.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Count enemies</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C#</td>
            <td class="dec-input">Person called p1</td>
            <td class="dec-input">Person called p2</td>
            <td class="dec-input">p1 hates p2</td>
            <td class="dec-output">number hated persons of p1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">yes</td>
            <td class="dec-td">p2</td>
        </tr>
    </table>
    <br>
    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Noone hates all</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Person</td>
            <td class="dec-output">number hated persons of Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&lt #Person</td>
        </tr>
    </table>
    <br>


.. admonition:: The end

    **Run the example.**

Now that all the rules are encoded in tables, we can run the example and discover the mystery of Dreadsbury mansion!
Running the solver on our created model results in the following:
 
.. code:: bash

    Number of models: 1
    Model 1
    =======
    structure  : V {
        Person_hates_Person = { Agatha,Agatha; Agatha,Charles; Butler,Agatha; Butler,Charles; Charles,Butler }
        Person_richer_than_Person = { Butler,Agatha }
        Hated_persons = { Agatha->2; Butler->2; Charles->1 }
        Killer = Agatha
    }

    Elapsed Time:
    0.09528

It turns out Agatha hates herself, and was her own killer!
