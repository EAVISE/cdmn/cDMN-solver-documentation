.. _balanced_assignment:

Balanced Assignment
-------------------

This example is the `Balanced Assignment <https://dmcommunity.org/challenge/challenge-sep-2018/>`_ challenge, in which we need to create groups that are as diverse as possible.

.. admonition:: Balanced Assignment

    **Given several employee categories (title, location, department, male/female, ...) and a specified number of groups, assign every employee to a project group so that the groups are the same size, and they are as diverse as possible.**


Together with this specification, we are given a list of 210 people and their information.

This challenge is an interesting optimization challenge.
As always, we start by filling out the glossary.
We create a type to represent the people, and types for every information type we have.
Then, we also need a type which will represent a number of persons, and a type which will just represent a large integer.

Note here that because our list has 210 entries, it would be nearly impossible to add all of these into the *Values* column of the glossary.
We opt here to not define the values, but instead we will point the glossary to our data table, which will tell the solver that every value in the data table is a possible value.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Person</td>
            <td class="glos-td">String</td>
            <td class="glos-td">See Data Table Person</td>
        </tr>
        <tr>
            <td class="glos-td">Group</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..12]</td>
        </tr>
        <tr>
            <td class="glos-td">Department</td>
            <td class="glos-td">string</td>
            <td class="glos-td">See Data Table Person</td>
        </tr>
        <tr>
            <td class="glos-td">Location</td>
            <td class="glos-td">string</td>
            <td class="glos-td">See Data Table Person</td>
        </tr>
        <tr>
            <td class="glos-td">Gender</td>
            <td class="glos-td">string</td>
            <td class="glos-td">See Data Table Person</td>
        </tr>
        <tr>
            <td class="glos-td">Title</td>
            <td class="glos-td">string</td>
            <td class="glos-td">See Data Table Person</td>
        </tr>
        <tr>
            <td class="glos-td">BoundNumber</td>
            <td class="glos-td">int</td>
            <td class="glos-td">[0, 1000000]</td>
        </tr>
        <tr>
            <td class="glos-td">NbPersons</td>
            <td class="glos-td">int</td>
            <td class="glos-td">[16..19]</td>
        </tr>
    </table>
    <br>


Next up, we introduce a function for every parameter, which will assign every person to their info.
We will also need a function which maps every group on a number, representing the number of people in that group.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">department of Person</td>
            <td class="glos-td">Department</td>
        </tr>
        <tr>
            <td class="glos-td">location of Person</td>
            <td class="glos-td">Location</td>
        </tr>
        <tr>
            <td class="glos-td">gender of Person</td>
            <td class="glos-td">Gender</td>
        </tr>
        <tr>
            <td class="glos-td">title of Person</td>
            <td class="glos-td">Title</td>
        </tr>
        <tr>
            <td class="glos-td">group of Person</td>
            <td class="glos-td">Nat</td>
        </tr>
        <tr>
            <td class="glos-td">number of Group</td>
            <td class="glos-td">NbPersons</td>
        </tr>
    </table>
    <br>

To finish our glossary, we introduce a constant *Score* which will keep track of the score of a solution.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Score</td>
            <td class="glos-td">BoundNumber</td>
        </tr>
    </table>
    <br>

With our glossary finished, we can move on to the next step.
We need a way to calculate the **Score** value for a solution.
To do this, we will check for every two people that if they have something in common, that they are in a different group.
If this is the case, then we add 1 to our score.



.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="8">Calculate Score</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Person called p1</td>
            <td class="dec-input">Person called p2</td>
            <td class="dec-input">department of p1</td>
            <td class="dec-input">location of p1</td>
            <td class="dec-input">gender of p1</td>
            <td class="dec-input">title of p1</td>
            <td class="dec-input">group of p1</td>
            <td class="dec-output">Score</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">= department of p2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(group of p2)</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">= location of p2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(group of p2)</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">= gender of p2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(group of p2)</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">= title of p2</td>
            <td class="dec-td">not(group of p2)</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

Now, we want to count the number of people per group.
Again, we can do this in a simple table.
``Number in group of Person`` is a combination of ``Number in Group`` and ``group of Person``.
This allows us to count the amount of people per group.
Now, because the type ``NbPersons`` is defined in our glossary as ``[16, 19]``, this automatically creates the constrain that every group needs to have between 16 and 19 people.

All that rests now is creating a ``Goal`` table in order to maximize the score!


.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Maximize Score</td>
        </tr>
    </table>
    <br>

And that's it!
Sadly, the cDMN solver currently is unable to completely solve this challenge, as it is quite large.
However, the cDMN notation is capable of fully modelling the problem, which is the main point.
