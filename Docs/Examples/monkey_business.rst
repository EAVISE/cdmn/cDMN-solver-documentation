.. _monkey_business:

Monkey Business
================

This page details the cDMN implementation of the Monkey Business challenge, as listed on https://dmcommunity.org/challenge/challenge-nov-2015/.
The challenge is as follows:

.. admonition:: Monkey Business

   Mrs. Robinson's 4th grade class took a field trip to the local zoo.
   The day was sunny and warm --- a perfect day to spend at the zoo.
   The kids had a great time and the monkeys were voted the class favorite animal.
   The zoo had four monkeys --- two males, and two females.
   It was lunchtime for the monkeys and as the kids watched, each one ate a different fruit in their favorite resting place:

   1. Sam, who doesn't like bananas, likes sitting on the grass.
   2. The monkey who sat on the rock ate the apple. The monkey who ate the pear didn't sit on the tree branch.
   3. Anna sat by the stream but she didn't eat the pear.
   4. Harriet didn't sit on the tree branch. Mike doesn't like oranges.

   **Question**: Can you determine the name of each monkey, what kind of fruit each monkey ate, and where their favorite resting place was?

The first thing we should do, is create the glossary.
We have three types: one for the monkeys, one for the places, and one for the fruits.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Monkey</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Anna, Sam, Harriet, Mike</td>
        </tr>
        <tr>
            <td class="glos-td">Place</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Grass, Rock, Branch, Stream</td>
        </tr>
        <tr>
            <td class="glos-td">Fruit</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Apple, Pear, Banana, Orange</td>
        </tr>
    </table>
    <br>


Now we need a way to assign a fruit and a place to each monkey.
Since every monkey needs exactly one fruit, and exactly one resting place, functions are perfectly suited for this task.
A function is a mapping of a set of arguments to a return value.
For instance, the function ``Place of Monkey`` will map each monkey on exactly one place.

.. raw:: html

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Place of Monkey</td>
            <td class="glos-td">Place</td>
        </tr>
        <tr>
            <td class="glos-td">Fruit of Monkey</td>
            <td class="glos-td">Fruit</td>
        </tr>
    </table>
    <br>

That's it!
Our glossary is fully done.
We can now focus on the logic aspect.

In the challenge description, there were two main pieces of info:

    1. The hints of which monkey ate what fruit where.
    2. The fact that no monkeys can have the same place or the same fruit.

Both of these are easily turned into constraint tables.
For instance, we can state that "For every monkey m1 and m2 (different from m1) must hold that they have a different Place and a different Fruit."

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Different fruit and places</th>
            <th class="dec-empty"></th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Monkey called m1</td>
            <td class="dec-input">Monkey called m2</td>
            <td class="dec-output">Place of m1</td>
            <td class="dec-output">Fruit of m1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(m1)</td>
            <td class="dec-td">not(Place of m2)</td>
            <td class="dec-td">not(Fruit of m2)</td>
        </tr>
    </table>
    <br>

Creating a table for the other piece of info is a bit more work, but is still fairly simple.
The full table containing all snippets of information looks like this:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Monkey information</th>
            <th class="dec-empty"></th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Monkey</td>
            <td class="dec-input">Place of Monkey</td>
            <td class="dec-input">Fruit of Monkey</td>
            <td class="dec-output">Place of Monkey</td>
            <td class="dec-output">Fruit of Monkey</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Sam</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Grass</td>
            <td class="dec-td">not(Banana)</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Rock</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Apple</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Pear</td>
            <td class="dec-td">not(Branch)</td>
            <td class="dec-td">-</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Anna</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Stream</td>
            <td class="dec-td">not(Pear)</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">Harriet</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(Branch)</td>
            <td class="dec-td">-</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">Mike</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(Orange)</td>
        </tr>
    </table>
    <br>

Rule 1 states that Sam sat on the Grass, and didn't eat the Banana.
Similarly, rule 2 states that the monkey who sat on the rock, ate the Apple.
Every row in this constraint table represents a snippet of information.

And that's it! Using two glossary tables, and two constraint tables, we were able to solve the challenge.
Running the cDMN solver gives us the following output:

.. code:: bash

    Model 1
    =======
    structure  : V {
      Fruit = { Anna->Orange; Harriet->Apple; Mike->Banana; Sam->Pear }
      Place = { Anna->Stream; Harriet->Rock; Mike->Branch; Sam->Grass }
    }
