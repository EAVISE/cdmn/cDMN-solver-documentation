.. _map_coloring_advanced:

Map Coloring With Violations
----------------------------

This version of the map coloring problem is an advanced version.
Instead of having four colors, we only have three.
In other words, some countries will have to share a color with a bordering country.
The full DMCommunity challenge, with other submitted solutions, can be found `here. <https://dmcommunity.org/challenge/challenge-june-2019/>`_

.. admonition:: Map Coloring with Violations

    This challenge is an advanced version of the May-2019 Challenge.
    Previously you had 4 colors (blue, red, green, or yellow) but now you have only 3 colors (blue, red, green).
    It is not enough to color six European countries: Belgium, Denmark, France, Germany, Luxembourg, and the Netherlands in such a way that no neighboring countries use the same color.
    So, some neighboring counties may have the same colors but there is a relative cost for such violations:

    France – Luxembourg: $257

    Luxembourg – Germany: $904

    Luxembourg – Belgium: $568

This problem is a bit harder than the previous one, because we can no longer create a constraint that bordering countries can not share a color.
Instead, we express this constraint only for all countries that are not Luxembourg.

We start by creating our glossary.
We create a type to represent countries and a type to represent the colors.
We also add a type *Number* to represent the violation score.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Country</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Belgium, France, Germany, Luxembourg, Netherlands</td>
        </tr>
        <tr>
            <td class="glos-td">Color</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Green, Red, Yellow</td>
        </tr>
        <tr>
            <td class="glos-td">Number</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..10000]</td>
        </tr>
    </table>
    <br>

To assign every country exactly one color, we can use a function.
We also need a constant to represent the *score* of a solution, which we will call *Violations*
To express that two countries border, we use a relation.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">color of Country</td>
            <td class="glos-td">Color</td>
        </tr>
    </table>
    <br>

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Violations</td>
            <td class="glos-td">Number</td>
        </tr>
    </table>
    <br>

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Country borders Country</td>
        </tr>
    </table>
    <br>

Now that our glossary is done, we move on to the next step.
We change our constraint table from the previous model into the following: if two countries border of which neither is Luxembourg, they cannot share the same color.
This way, Luxembourg is allowed to share a color with its bordering countries.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Bordering countries cannot share colors</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Country called c1</td>
            <td class="dec-input">Country called c2</td>
            <td class="dec-input">c1 borders c2</td>
            <td class="dec-output">color of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">not(Luxembourg)</td>
            <td class="dec-td">not(Luxembourg)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Not(color of c2)</td>
        </tr>
    </table>
    <br>

Now we add a table which will sum the weights of the vialotions, and store it in the *Violations* constant.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Coloring Violations</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">color of Luxembourg</td>
            <td class="dec-output">Violations</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">= color of France</td>
            <td class="dec-td">257</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">= color of Germany</td>
            <td class="dec-td">904</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">= color of Belgium</td>
            <td class="dec-td">568</td>
        </tr>
    </table>
    <br>

And that is all the logic we need to solve this problem!
We simply need to represent which countries border which using a data table, and then tell the system to minimize the *Violations* constant.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Data Table: Bordering Countries</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-input">Country called c1</td>
            <td class="dec-input">Country called c2</td>
            <td class="dec-output">c1 borders c2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Belgium</td>
            <td class="dec-td">France, Luxembourg, Netherlands, Germany</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Netherlands</td>
            <td class="dec-td">Germany</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Germany</td>
            <td class="dec-td">France, Luxembourg, Denmark</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">France</td>
            <td class="dec-td">Luxembourg</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Minimize Violations</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
This results in the following output:

.. code:: bash

    Number of models: 1
    Model 1
    =======
    structure  : V {
      Country_borders_Country = { Belgium,France; Belgium,Germany; Belgium,Luxembourg; Belgium,Netherlands; France,Luxembourg; Germany,Denmark; Germany,France; Germany,Luxembourg; Netherlands,Germany }
      Color = { Belgium->Red; Denmark->Red; France->Green; Germany->Yellow; Luxembourg->Green; Netherlands->Green }
      Violations = 257
    }

    Elapsed Time:
    0.047935

And there we have it!
If Luxembourg only shares a color with France, we have the lowest score for violation.

Of course, this is a trivial example: this answer was visible from the start.
Instead of only having a violation for Luxembourg, we could create scores for every two bordering countries and then minimize it.
This can be done by removing the constraint table, and changing the **C+** table to a table which counts the violation weight for every two countries if they share the same color.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Coloring Violations</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Country called c1</td>
            <td class="dec-input">Country called c2</td>
            <td class="dec-input">color of c1</td>
            <td class="dec-output">Violations</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">color of c2</td>
            <td class="dec-td">Weight of c1 and c2</td>
        </tr>
    </table>
    <br>

Where *Weight of Country and Country* is a function which maps every pair of countries on a violation weight, which is defined in a data table.
