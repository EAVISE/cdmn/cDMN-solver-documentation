.. _department_employees:

Classify Department Employees
-----------------------------

Classify Department Employees, another challenge from `dmcommunity.org <https://dmcommunity.org/>`_, has long been an impossible challenge for our cDMN solver.
While cDMN is 100% capable of modeling the problem (we already modeled it in 2020), the solver could not handle actually solving the challenge for two reasons: the occurence of large numbers, and the need for division.
However, since cDMN version `2.0.0` we have a new internal solver, `IDP-Z3 <https://www.IDP-Z3.be/>`_, and we can now effectively tackle this challenge!


.. admonition:: Classify Department Employees Challenge


    Hier de uitleg van de challenge.

.. csv-table::
    :header: "Person", "Marital Status", "Gender", "Age", "Salary"

    "Robinson", "Married", "Female", "25", "20000"
    "Warner", "Married", "Male", "45", "150000"
    "Stevens", "Single", "Male", "24", "35000"
    "...", "...", "...", "...", "..."

We start by creating a glossary for our personnel and their attributes.
For each of our string-based attributes, we will introduce a type.
For the numerical attiributes, we do not need to set a range of values and instead use the generic type `Real`.

.. note::

    Before cDMN version 2.0.0, using supertypes `int` and `real` directly was not allowed.
    Note that they cannot be used everywhere: e.g., they cannot be used as arguments of functions or predicates.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Person</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Robinson, Warner, Stevens, ...</td>
        </tr>
        <tr>
            <td class="glos-td">Marital_Status</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Married, Single</td>
        </tr>
        <tr>
            <td class="glos-td">Gender</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Male, Female</td>
        </tr>
    </table>
    <br>

Next, we need a way to represent which attribute values belong to which people.
This is a quite straighforward example for functions: every person has exactly one value for each parameter.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">status of Person</td>
            <td class="glos-td">Marital_Status</td>
        </tr>
        <tr>
            <td class="glos-td">gender of Person</td>
            <td class="glos-td">Gender</td>
        </tr>
        <tr>
            <td class="glos-td">age of Person</td>
            <td class="glos-td">Real</td>
        </tr>
        <tr>
            <td class="glos-td">salary of Person</td>
            <td class="glos-td">Real</td>
        </tr>
        <tr>
            <td class="glos-td">Number of Item</td>
            <td class="glos-td">Nat</td>
        </tr>
    </table>
    <br>

The aim of the challenge is calculate values such as the minimal value, maximum value, and more based on our set of employees.
To get the result of these calculations, we use constants to "store" them.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Minimal Salary</td>
            <td class="glos-td">Real</td>
        </tr>
        <tr>
            <td class="glos-td">Maximal Salary</td>
            <td class="glos-td">Real</td>
        </tr>
        <tr>
            <td class="glos-td">Average Salary</td>
            <td class="glos-td">Real</td>
        </tr>
        <tr>
            <td class="glos-td">Total Salary</td>
            <td class="glos-td">Real</td>
        </tr>
    </table>
    <br>


Finally, we need a way to keep track of which people are rich
For this, a simple relation will work best.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Person is rich</td>
        </tr>
    </table>
    <br>

Now that our glossary is complete, we can start figuring out how to best express our knowledge.
Let's start with the minimal, maximal and total salary.
Thanks to cDMN's aggregates (TODO: link naar aggregaten), this is very straightforward: the `C<`, `C>` and `C+` hit policies respectively calculate the minimum, maximum and total of all rules that fire.
For example, for the minimum, this looks as follows:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Minimun</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C<</td>
            <td class="dec-input">Person</td>
            <td class="dec-output">Minimal Salary</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-input">-</td>
            <td class="dec-output">salary of Person</td>
        </tr>
    </table>
    <br>

In words, this table is quite simple: "Find the minimal salary of all persons" (or: for each person, find the minimum salary).
For the two other values, the tables are virtually the same.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Maximum</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C></td>
            <td class="dec-input">Person</td>
            <td class="dec-output">Maximal Salary</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-input">-</td>
            <td class="dec-output">salary of Person</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Total</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C#</td>
            <td class="dec-input">Person</td>
            <td class="dec-output">Total Salary</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-input">-</td>
            <td class="dec-output">salary of Person</td>
        </tr>
    </table>
    <br>

We can use the total salary to calculate the average salary.
Again, this is fairly straightforward: just divide the total salary by the total number of people.

.. admonition:: Number of domain elements of a type

    In this example, we need to know the number of elements that are in the type `Person`.
    cDMN has an easy way of representing this, using the `#Type` operator.
    In this example, `#Person` equals 12 and `#Marital_Status` equals 2.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Avg</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-output">Average Salary</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Total Salary/#Person</td>
        </tr>
    </table>
    <br>

The finaly thing that we need to do, is figure out which people are rich.
According to the challenge, everyone with a salary over 85000 is considered rich.
So, we express in a table that "Each person with a salary over 85000 is rich".

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Rich Employees</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">salary of Person</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td"&#gt 85000</td>
        </tr>
    </table>
    <br>

Our model is now complete, and we can run it using the cDMN solver.
This results in the following output:

.. code:: bash

    TODO

We are able to make a burger which only costs 72, and still meets (meats? :-) ) all the requirements.
