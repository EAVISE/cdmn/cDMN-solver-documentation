.. _doctor_planning:

Doctor Planning
---------------

The Doctor Planning example was submitted by us to the `dmcommunity.org <https://dmcommunity.org/>`_ as a challenge.
The full version, together with other submitted solutions, can be found on `this page <https://dmcommunity.org/challenge/challenge-apr-2020/>`_.
Its specification is as follows:

.. admonition:: Doctor Planning


    In a hospital, there has to be a doctor present at all times.
    In order to make sure this is the case, a planning is made for the next 7 days, starting on a monday.
    Each day consists of three shifts: an early shift, a late shift and a night shift.

    Every shift needs to be assigned to a doctor.
    In total there are 5 doctors: every doctor has a list of available days, and some have special requirements.
    In general, the following rules apply:

    1. A doctor can only work one shift per day.
    2. A doctor should always be available for his shift (see table below)
    3. If a doctor has the night shift, they either get the next day off, or the night shift again.
    4. A doctor either works both days of the weekend, or none of the days.

    **A planning should be made in which every requirement is fulfilled.**

.. csv-table::
    :header: "Name", "Available"

    "Fleming", "Friday, Saturday, Sunday"
    "Freud", "Every day early or late, never night"
    "Heimlich", "Every day but neven the night shift on weekends"
    "Eustachi", "Every day, every shift"
    "Golgi", "Every day, every shift but at max 2 night shifts"

To create a cDMN model for this challenge, we start by building our glossary.
From reading the description, we can see that we will need the following types:

* Doctor
* Day
* Time (early, late or night)
* Nb_shifts, to represent a number of shifts.

Thus, we create a glossary table as such:

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Doctor</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Fleming, Freud, Heimlich, Eustachi, Golgi</td>
        </tr>
        <tr>
            <td class="glos-td">Day</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday</td>
        </tr>
        <tr>
            <td class="glos-td">Time</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Early, Late, Night</td>
        </tr>
        <tr>
            <td class="glos-td">Nb Shift</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..21]</td>
        </tr>
    </table>
    <br>

Next, we will need a way to assign doctors to days and shifts.
Since for every pair of day and shift (e.g. "Monday" and "Early") we need exactly 1 doctor, we use a function to do so.
We also introduce functions to count the number of shifts per doctor and day, and the number of night shifts per doctor.
Finally, we create one last function to represent the next day for every day.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">doctor assigned to Day and Time</td>
            <td class="glos-td">Doctor</td>
        </tr>
        <tr>
            <td class="glos-td">nb shifts of Doctor and Day</td>
            <td class="glos-td">Nb_Shift</td>
        </tr>
        <tr>
            <td class="glos-td">nb nights of Doctor</td>
            <td class="glos-td">Nb_Shift</td>
        </tr>
        <tr>
            <td class="glos-td">day after Day</td>
            <td class="glos-td">Day</td>
        </tr>
    </table>
    <br>

The last information to include in our model is the availabilities of every doctor.
For this, we implement the relation *Doctor is available on Day and Time*.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Doctor is available on Day and Time</td>
        </tr>
    </table>
    <br>

Now that our glossary is done, we can move on to implementing the rules.
We start by first implementing every rule one by one.

.. admonition:: Rule 1

   **A doctor can only work one shift per day.**

This rule can be modeled as a simple constraint table.
We evaluate every doctor, on every day, and state that they work at max 1 shift that day.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Rule 1</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day</td>
            <td class="dec-output">nb shifts of Doctor and Day</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&le; 1</td>
        </tr>
    </table>
    <br>


.. admonition:: Rule 2

    **A doctor should always be available for his shift.**

Once again, a single constraint table suffices.
We state that for every doctor, day, and time, IF the doctor is assigned to that day and time, THEN he has to be available.
In other words, a doctor can only be assigned to a day and time if he is available.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="5">Rule 2</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day</td>
            <td class="dec-input">Time</td>
            <td class="dec-input">doctor assigned to Day and Time</td>
            <td class="dec-output">Doctor is available on Day and Time</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Doctor</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule 3
    
    **If a doctor has the night shift, they either get the next day off, or the night shift again.**

This rule is by far the most difficult one to implement.
We go over every doctor who works the night shift, and then state that they cannot be assigned to the early or late shift on the next day.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">Rule 3</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day called d1</td>
            <td class="dec-input">doctor assigned to d1 and Night</td>
            <td class="dec-input">Day called d2</td>
            <td class="dec-input">Time</td>
            <td class="dec-output">doctor assigned to d2 and Time</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Doctor</td>
            <td class="dec-td">Day after d1</td>
            <td class="dec-td">not(Night)</td>
            <td class="dec-td">not(Doctor)</td>
        </tr>
    </table>
    <br>
    

.. admonition:: Rule 4

    **A doctor either works both days of the weekend, or none of the days.**

In order to implement this rule, we create a constraint table that says that every doctor who has a shift on Saturday has to work on Sunday, and vice versa.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="5">Rule 4</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day called d1</td>
            <td class="dec-input">nb shifts of Doctor and d1</td>
            <td class="dec-input">Day called d2</td>
            <td class="dec-output">nb shifts of Doctor and d2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Saturday</td>
            <td class="dec-td">1</td>
            <td class="dec-td">Sunday</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Sunday</td>
            <td class="dec-td">1</td>
            <td class="dec-td">Saturday</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

.. admonition:: Special preference

    **Golgi only works a maximum of 2 night shifts every week.**

This preference nicely shows of the simplicity of constraints in cDMN.
We implement it as follows:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Max Nb of Nights</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-output">nb nights of Doctor</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Golgi</td>
            <td class="dec-td">&le; 2</td>
        </tr>
    </table>
    <br>

Now that every rule has been implemented, we need to create tables for the other concepts which we defined.
We start by creating the decision table which defines the next day for every day.
Then we create the tables which count the number of shifts per day, and the number of night shifts per week.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Define next day</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Day</td>
            <td class="dec-output">daf after Day</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Monday</td>
            <td class="dec-td">Tuesday</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Tuesday</td>
            <td class="dec-td">Wednesday</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Wednesday</td>
            <td class="dec-td">Thursday</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Thursday</td>
            <td class="dec-td">Friday</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">Friday</td>
            <td class="dec-td">Saturday</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">Saturday</td>
            <td class="dec-td">Sunday</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">Sunday</td>
            <td class="dec-td">Monday</td>
        </tr>
    </table>
    <br>


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="5">Count shifts per day</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day</td>
            <td class="dec-input">Time</td>
            <td class="dec-input">doctor assigned to Day and Time</td>
            <td class="dec-output">nb shifts of Doctor and Day</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Doctor</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Count number of night shifts per doctor</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day</td>
            <td class="dec-input">doctor assigned to Day and Night</td>
            <td class="dec-output">nb nights of Doctor</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Doctor</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

Now all that rests is implementing the availablities in a data table.
This table is quite long, so only the first few lines are shown.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Data Table: Availabilities</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-input">Doctor</td>
            <td class="dec-input">Day</td>
            <td class="dec-input">Time</td>
            <td class="dec-output">Doctor is available on Day and Time</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Fleming</td>
            <td class="dec-td">Friday</td>
            <td class="dec-td">Early</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Fleming</td>
            <td class="dec-td">Friday</td>
            <td class="dec-td">Late</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Fleming</td>
            <td class="dec-td">Friday</td>
            <td class="dec-td">Night</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
For example, this is one of the solutions found by the solver:

.. code:: bash

    Model 1
    =======
    structure  : V {
      Doctor_is_available_on_Day_and_Time = { Eustachi,Friday,Early; Eustachi,Friday,Late; Eustachi,Friday,Night; Eustachi,Monday,Early; Eustachi,Monday,Late; Eustachi,Monday,Night; Eustachi,Saturday,Early; Eustachi,Saturday,Late; Eustachi,Saturday,Night; Eustachi,Sunday,Early; Eustachi,Sunday,Late; Eustachi,Sunday,Night; Eustachi,Thursday,Early; Eustachi,Thursday,Late; Eustachi,Thursday,Night; Eustachi,Tuesday,Early; Eustachi,Tuesday,Late; Eustachi,Tuesday,Night; Eustachi,Wednesday,Early; Eustachi,Wednesday,Late; Eustachi,Wednesday,Night; Fleming,Friday,Early; Fleming,Friday,Late; Fleming,Friday,Night; Fleming,Saturday,Early; Fleming,Saturday,Late; Fleming,Saturday,Night; Fleming,Sunday,Early; Fleming,Sunday,Late; Fleming,Sunday,Night; Freud,Friday,Early; Freud,Friday,Late; Freud,Monday,Early; Freud,Monday,Late; Freud,Saturday,Early; Freud,Saturday,Late; Freud,Sunday,Early; Freud,Sunday,Late; Freud,Thursday,Early; Freud,Thursday,Late; Freud,Tuesday,Early; Freud,Tuesday,Late; Freud,Wednesday,Early; Freud,Wednesday,Late; Golgi,Friday,Early; Golgi,Friday,Late; Golgi,Friday,Night; Golgi,Monday,Early; Golgi,Monday,Late; Golgi,Monday,Night; Golgi,Saturday,Early; Golgi,Saturday,Late; Golgi,Saturday,Night; Golgi,Sunday,Early; Golgi,Sunday,Late; Golgi,Sunday,Night; Golgi,Thursday,Early; Golgi,Thursday,Late; Golgi,Thursday,Night; Golgi,Tuesday,Early; Golgi,Tuesday,Late; Golgi,Tuesday,Night; Golgi,Wednesday,Early; Golgi,Wednesday,Late; Golgi,Wednesday,Night; Heimlich,Friday,Early; Heimlich,Friday,Late; Heimlich,Friday,Night; Heimlich,Monday,Early; Heimlich,Monday,Late; Heimlich,Monday,Night; Heimlich,Saturday,Early; Heimlich,Saturday,Late; Heimlich,Sunday,Early; Heimlich,Sunday,Late; Heimlich,Thursday,Early; Heimlich,Thursday,Late; Heimlich,Thursday,Night; Heimlich,Tuesday,Early; Heimlich,Tuesday,Late; Heimlich,Tuesday,Night; Heimlich,Wednesday,Early; Heimlich,Wednesday,Late; Heimlich,Wednesday,Night }
      Assigned_Doctor = { Friday,Early->Eustachi; Friday,Late->Golgi; Friday,Night->Fleming; Monday,Early->Freud; Monday,Late->Golgi; Monday,Night->Heimlich; Saturday,Early->Heimlich; Saturday,Late->Golgi; Saturday,Night->Eustachi; Sunday,Early->Golgi; Sunday,Late->Heimlich; Sunday,Night->Eustachi; Thursday,Early->Eustachi; Thursday,Late->Freud; Thursday,Night->Heimlich; Tuesday,Early->Eustachi; Tuesday,Late->Freud; Tuesday,Night->Heimlich; Wednesday,Early->Freud; Wednesday,Late->Golgi; Wednesday,Night->Heimlich }
      Nb_Nights = { Eustachi->2; Fleming->1; Freud->0; Golgi->0; Heimlich->4 }
      Nb_Shifts = { Eustachi,Friday->1; Eustachi,Monday->0; Eustachi,Saturday->1; Eustachi,Sunday->1; Eustachi,Thursday->1; Eustachi,Tuesday->1; Eustachi,Wednesday->0; Fleming,Friday->1; Fleming,Monday->0; Fleming,Saturday->0; Fleming,Sunday->0; Fleming,Thursday->0; Fleming,Tuesday->0; Fleming,Wednesday->0; Freud,Friday->0; Freud,Monday->1; Freud,Saturday->0; Freud,Sunday->0; Freud,Thursday->1; Freud,Tuesday->1; Freud,Wednesday->1; Golgi,Friday->1; Golgi,Monday->1; Golgi,Saturday->1; Golgi,Sunday->1; Golgi,Thursday->0; Golgi,Tuesday->0; Golgi,Wednesday->1; Heimlich,Friday->0; Heimlich,Monday->1; Heimlich,Saturday->1; Heimlich,Sunday->1; Heimlich,Thursday->1; Heimlich,Tuesday->1; Heimlich,Wednesday->1 }
      Day_After = { Friday->Saturday; Monday->Tuesday; Saturday->Sunday; Sunday->Monday; Thursday->Friday; Tuesday->Wednesday; Wednesday->Thursday }
    }

    Elapsed Time:
    0.104035

The *Assigned_Doctor* function maps every day and shift on a doctor, conform to all the requirements!
