.. _calculator:

Calculator with Two Buttons
---------------------------

The following problem was posed as the `DMCommunity November 2020 challenge. <https://dmcommunity.org/challenge/challenge-nov-2020/>`_

.. admonition:: Calculator with Two Buttons

   A calculator, initially displaying 0, has only two buttons:

   * "+" adds 1 to the number on the display;
   * "x" multiplies the number on the display by 10.

   What is the least number of button presses needed to show 5034?

This problem can easily be solved by hand, but it is always fun to create a cDMN specification for these types of problems.

As always, we start by creating a glossary.
We need a type to represent presses, the two types of buttons and the value of the number.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Press</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..20]</td>
        </tr>
        <tr>
            <td class="glos-td">Button</td>
            <td class="glos-td">String</td>
            <td class="glos-td">add, mult</td>
        </tr>
        <tr>
            <td class="glos-td">Value</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..6000]</td>
        </tr>
    </table>
    <br>

For every press, we need to represent what button was pressed and what value the display showed beforehand.
To do this, we create two functions, `button of Press` and `value of Press`.
In this way, if `button of 5 = mult`, this would mean that the sixth button pressed (starting at zero) was a multiplication.
Similarly for `value of 5 = 4`, then the value before the sixth button was pressed is 4.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">button of Press</td>
            <td class="glos-td">Button</td>
        </tr>
        <tr>
            <td class="glos-td">value of Press</td>
            <td class="glos-td">Value</td>
        </tr>
    </table>
    <br>

To represent the number of presses needed to reach our goal of 5034, we introduce a constant called `NbPresses`.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">NbPresses</td>
            <td class="glos-td">Press</td>
        </tr>
    </table>
    <br>

With our glossary done, we can start constructing tables.
The first table we add is a constraint table in order to specify that our starting number is zero, by fixing the value of the first press.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Starting value</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">value of 0</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">0</td>
        </tr>
    </table>
    <br>

After doing so, we add the constraints stating how the value changes when a button is pressed.
The first rule of the below table states that when the `add` button is pressed, the next value is the previous value plus one.
The second rule states that when the `mult` button is pressed, the next value is the previous value times ten.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Calculate next value</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Press called p1</td>
            <td class="dec-input">button of p1</td>
            <td class="dec-input">Press called p2</td>
            <td class="dec-output">value of p2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">add</td>
            <td class="dec-td">p1 + 1</td>
            <td class="dec-td">value of p1 + 1 </td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">mult</td>
            <td class="dec-td">p1 + 1</td>
            <td class="dec-td">value of p1 * 10 </td>
        </tr>
    </table>
    <br>

Now we define what that the value of `NbPresses` is the press when the value is 5034.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Define NbPress</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Press</td>
            <td class="dec-input">value of Press</td>
            <td class="dec-output">NbPresses</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">5034</td>
            <td class="dec-td">Press</td>
        </tr>
    </table>
    <br>

Now that we have all the required logic in place, we simply need to tell the system that we want to minimize the number of presses via the ``Goal`` table.

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Minimize NbPresses</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
This results in the following output:

.. code:: bash

    Number of models: 1
    Model 1
    =======
    structure  : V {
      Button = { 0->add; 1->add; 2->add; 3->add; 4->add; 5->mult; 6->mult; 7->add; 8->add; 9->add; 10->mult; 11->add; 12->add; 13->add; 14->add; 15->add; 16->add; 17->add; 18->add; 19->add; 20->add }
      NbPresses = 15
      Value = { 0->0; 1->1; 2->2; 3->3; 4->4; 5->5; 6->50; 7->500; 8->501; 9->502; 10->503; 11->5030; 12->5031; 13->5032; 14->5033; 15->5034; 16->5035; 17->5036; 18->5037; 19->5038; 20->5039 }
    }

    Elapsed Time:
    0.312465

The number 5034 can be reached in 15 presses!
