.. _vacation_days:

Vacation Days
-------------

This example is another challenge taken from `dmcommunity.org <https://dmcommunity.org/>`_, namely `Vacation Days Calculation <https://dmcommunity.org/challenge/challenge-jan-2016/>`_.
In this challenge, we need to calculate vacation days for employees, based on their age and years of service.
The description is as follows:

.. admonition:: Vacation Days

    The number of vacation days depends on age and years of service.

    Every employee receives at least 22 days.
    Additional days are provided by the following criteria:

    1) Only employees younger than 18 or at least 60 years, or employees with at least 30 years of service will receive 5 extra days.

    2) Employees with at least 30 years of service and also employees of age 60 or more, receive 3 extra days, on top of possible additional days already given.

    3) If an employee has at least 15, but less than 30 years of service, 2 extra days are given. These 2 days are also provided for employees of age 45 or more. These 2 extra days can not be combined with the 5 extra days.

To solve this challenge, there's two main ways:

    1. Convert all these rules to one big ``C+`` table, and count the number of days for each employee.
    2. Convert each rule into its own table and relation, and then count the days at the end for each rule satisfied.

Both have been implemented in our available `cDMN.xlsx <https://gitlab.com/EAVISE/cDMN/-/raw/master/Data/DMChallenges.xlsx>`_ , but only the second one will be explained here.
By first checking which rules are satisfied and then calculating the days, we gain readability and traceability in our implementation.

We start modeling the example by building the glossary.
First of all, we need one or more types representing the number of vacation days, the age and the service years. 
We set the range for vacation days between 22 and 32, as these are the theoretical minimum and maximum.
For age and service years, we pick between 0 and 100.
Remember, choosing a well defined range can be a major improvement in speed of the solver.
On the flipside, if your range does not include a value which is needed, errors will show up.

In addition, we will also need a type to represent employees.
For the example, we create three dummy employees: Huey, Dewey and Louie.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Vacation Days</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[22..32]</td>
        </tr>
        <tr>
            <td class="glos-td">Age</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
        <tr>
            <td class="glos-td">Service Years</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
        <tr>
            <td class="glos-td">Employee</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Huey, Dewey, Louie</td>
        </tr>
    </table>
    <br>

After creating the types, we can move on to the next sub-glossary.
We need a way to map each employee on their information: since every employee can only have exactly one age, amount of service years and amount of vacation days, we opt for functions.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">age of Employee</td>
            <td class="glos-td">Age</td>
        </tr>
        <tr>
            <td class="glos-td">service years of Employee</td>
            <td class="glos-td">Service Years</td>
        </tr>
        <tr>
            <td class="glos-td">vacation days of Employee </td>
            <td class="glos-td">Vacation Days</td>
        </tr>
    </table>
    <br>
    

Thirdly, we need a way to express which rules are satisfied for which employee.
For this purpose, we introduce a relation for each rule.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is eligible for r1</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is eligible for r2</td>
        </tr>
        <tr>
            <td class="glos-td">Employee is eligible for r3</td>
        </tr>
    </table>
    <br>

Now that our glossary is complete, we can start creating decision and constraint tables.
The first tables we should make, are the ones for the extra vacation days.
There's three such rules, and thus, three such tables.


.. admonition:: Rule 1
    
    **Only employees younger than 18 or at least 60 years, or employees with at least 30 years of service will receive 5 extra days.**

The easiest way to model this rule (and the following ones) is by using a ``U`` table.
The rows of the table can be translated to the following:

1. Every employee younger than 18 years is eligible.
2. Every employee over the age of 60 is eligible.
3. Every employee between 18 or 60 with at least 30 years of service is eligible.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Rule 1</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&lt 18</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&gt= 60</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[18, 60)</td>
            <td class="dec-td">&gt= 30</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule 2

    **Employees with at least 30 years of service and also employees of age 60 or more, receive 3 extra days, on top of possible additional days already given.**

We opt for another ``U`` table here.
It is similar to the previous table: it uses the same input columns, but a different output column.
The rows can be translated to the following:

1. Everyone with more than 30 service years is eligible for rule 2.
2. Everyone older than 60, with less than 30 years of service are also eligible.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Rule 2</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&gt= 30</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&gt= 60</td>
            <td class="dec-td">&lt 30</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule

    **If an employee has at least 15 but less than 30 years of service, 2 extra days are given.
    These 2 days are also provided for employees of age 45 or more.
    These 2 extra days can not be combined with the 5 extra days.**

Again, the ``U`` table suits our needs perfectly.
We ignore the last sentence of the rule, as we can implement this later.
The rows in the following table can be translated as such:

1. Everyone younger than 45 with at least 15 but at maximum 30 service years is eligible.
2. Everyone older than 45 is also eligible. 

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Rule 3</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">age of Employee</td>
            <td class="dec-input">service years of Employee</td>
            <td class="dec-output">Employee is eligible for r3</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&lt 45</td>
            <td class="dec-td">[15, 30)</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&gt 45</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

Now all that we need to do is add all the days of every applicable rule together, and count them in a ``C+`` table.
This table evaluates every row, and sums together the outputs of the satisfied rows.
Here we express in row 4 that the 2 days of rule 3 are only counted if the employee is not eligible for rule 1.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="5">Vacation days of Employee</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Employee</td>
            <td class="dec-input">Employee is eligible for r1</td>
            <td class="dec-input">Employee is eligible for r2</td>
            <td class="dec-input">Employee is eligible for r3</td>
            <td class="dec-output">vacation days of Employee</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">22</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">5</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">3</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">-</td>
            <td class="dec-td">No</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">2</td>
        </tr>
    </table>
    <br>


To now check the amount of vacation days our employees get, we can insert a data table containing their information, and run the solver.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Data Table Employee Info</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-input">Employee</td>
            <td class="dec-output">age of Employee</td>
            <td class="dec-output">service years of Employee</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Huey</td>
            <td class="dec-td">17</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Dewey</td>
            <td class="dec-td">70</td>
            <td class="dec-td">31</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Louie</td>
            <td class="dec-td">35</td>
            <td class="dec-td">16</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Donald</td>
            <td class="dec-td">46</td>
            <td class="dec-td">10</td>
        </tr>
    </table>
    <br>


This gives us the following solution:

.. code:: bash

   Number of models: 1
   Model 1
   =======
   structure  : V {
       Age = { Dewey->70; Donald->46; Huey->17; Louie->35 }
       Service_Years = { Dewey->31; Donald->10; Huey->1; Louie->16 }
       Vacation_Days = { Dewey->30; Donald->27; Huey->30; Louie->27 }
   }

   Elapsed Time:
   0.059808
