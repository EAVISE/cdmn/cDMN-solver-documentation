.. _community:

cDMN implementations of DMCommunity Challenges
==============================================


For our `publication on cDMN <https://arxiv.org/abs/2005.09998>`_, we looked at DMN challenges posted on the `DMCommunity <https://dmcommunity.org/>`_ website.
In total, we looked at 21 challenges.
Their cDMN implementations can be downloaded `here <https://gitlab.com/EAVISE/cdmn/cdmn-solver/-/raw/master/Examples/DMChallenges.xlsx>`_.
We created full explanations for some of the implementations, which can be found below.

.. csv-table:: DMN Challenges
    :header:  "Challenge", "Solved", "Link to the description"

    "Who killed Agatha?", "Yes", "https://dmcommunity.org/challenge/challenge-nov-2014/"
    "Change Making Decision", "Yes", "https://dmcommunity.org/challenge/challenge-feb-2015/"
    "Make a Good Burger", "Yes", "https://dmcommunity.org/challenge/make-a-good-burger/"
    "Define Duplicate Product Lines", "Yes", "https://dmcommunity.org/challenge/challenge-august-2015/"
    "Collection of Cars", "Yes", "https://dmcommunity.wordpress.com/challenge/challenge-sep-2015/"
    "Monkey Business", "Yes", "https://dmcommunity.org/challenge/challenge-nov-2015/"
    "Vacation Days", "Yes", "https://dmcommunity.wordpress.com/challenge/challenge-jan-2016/"
    "Greeting a Customer", "Yes", "https://dmcommunity.wordpress.com/challenge/challenge-aug-2016/"
    "Loan Approval", "No", "https://dmcommunity.org/challenge/challenge-dec-2016/"
    "Online Dating Services", "Yes", "https://dmcommunity.org/challenge/challenge-march-2017/"
    "Classify Department Employees", "Yes", "https://dmcommunity.org/challenge/challenge-sep-2017/"
    "Soldier Payment Rules", "No", "https://dmcommunity.org/challenge/challenge-nov-2017/"
    "Reindeer Ordering", "Yes", "https://dmcommunity.org/challenge/challenge-dec-2017/"
    "Zoo, Buses and Kids", "Yes", "https://dmcommunity.org/challenge/challenge-july-2018/"
    "Balanced Assignment", "Yes", "https://dmcommunity.org/challenge/challenge-sep-2018/"
    "Vacation Days Advanced", "Yes", "https://dmcommunity.org/challenge/challenge-nov-2018/"
    "Map Coloring", "Yes", "https://dmcommunity.org/challenge/challenge-may-2019/"
    "Map Coloring with Violations", "Yes", "https://dmcommunity.org/challenge-june-2019/"
    "Crack the Code", "Yes", "https://dmcommunity.org/challenge/challenge-sep-2019/"
    "Numerical Haiku", "Yes", "https://dmcommunity.org/challenge/challenge-nov-2019/"

Other DMCommunity Challenges
----------------------------

The solution file also contains other DM Community challenges, which weren't discussed in our paper.
In the future, more should be added.

.. csv-table:: Other DMN Challenges
    :Header: "Challenge", "Link to the description"

    "Nim Rules", "https://dmcommunity.org/challenge/challenge-jan-2020/"
    "Doctor Planning", "https://dmcommunity.org/challenge/challenge-apr-2020/"
    "Calculator with Two Buttons", "https://dmcommunity.org/challenge/challenge-nov-2020/"
    "Virtual Chess Tournament", "https://dmcommunity.org/challenge/challenge-dec-2020/"
    "Covid Testing", "https://dmcommunity.org/challenge/challenge-may-2021/"
    "Where is Gold", "https://dmcommunity.org/challenge/challenge-june-2021/"


Full implementations
--------------------

.. toctree::
   :maxdepth: 3
   :caption: Contents: 

   Examples/monkey_business
   Examples/change_making
   Examples/who_killed_agatha
   Examples/hamburger_challenge
   Examples/vacation_days
   Examples/vacation_days_advanced
   Examples/doctor_planning
   Examples/balanced_assignment
   Examples/map_coloring
   Examples/map_coloring_advanced
   Examples/crack_the_code
   Examples/zoo_buses_and_kids
   Examples/define_duplicate_product_lines
   Examples/calculator
   Examples/virtual_chess_tournament
   Examples/set
   Examples/covid_testing
   Examples/where_is_gold
   Examples/department_employees

